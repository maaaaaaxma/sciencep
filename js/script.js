function include(url) {
    document.write('<script src="' + url + '"></script>');
    return false;
}
/* DEVICE.JS
 ========================================================*/
include('js/device.min.js');

/* Stick up menu
 ========================================================*/
include('js/tmstickup.js');
$(window).load(function () {
    if ($('html').hasClass('desktop')) {
        $('#stuck_container').TMStickUp({
        })
    }
});

/* Easing library
 ========================================================*/
include('js/jquery.easing.1.3.js');


/* ToTop
 ========================================================*/
include('js/jquery.ui.totop.js');
$(function () {
    $().UItoTop({ easingType: 'easeOutQuart' });
});


/* DEVICE.JS AND SMOOTH SCROLLIG
 ========================================================*/
include('js/jquery.mousewheel.min.js');
include('js/jquery.simplr.smoothscroll.min.js');
$(function () {
    if ($('html').hasClass('desktop')) {
        $.srSmoothscroll({
            step: 150,
            speed: 800
        });
    }
});

/* Copyright Year
 ========================================================*/
var currentYear = (new Date).getFullYear();
$(document).ready(function () {
    $("#copyright-year").text((new Date).getFullYear());
});


/* Superfish menu
 ========================================================*/
include('js/superfish.js');
include('js/jquery.mobilemenu.js');

/* Unveil
 ========================================================*/
include('js/jquery.unveil.js');
$(document).ready(function () {
    $('img').unveil();
});

/* Orientation tablet fix
 ========================================================*/
$(function () {
// IPad/IPhone
    var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
        ua = navigator.userAgent,

        gestureStart = function () {
            viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0";
        },

        scaleFix = function () {
            if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
                viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
                document.addEventListener("gesturestart", gestureStart, false);
            }
        };

    scaleFix();
    // Menu Android
    if (window.orientation != undefined) {
        var regM = /ipod|ipad|iphone/gi,
            result = ua.match(regM)
        if (!result) {
            $('.sf-menu li').each(function () {
                if ($(">ul", this)[0]) {
                    $(">a", this).toggle(
                        function () {
                            return false;
                        },
                        function () {
                            window.location.href = $(this).attr("href");
                        }
                    );
                }
            })
        }
    }
});
var ua = navigator.userAgent.toLocaleLowerCase(),
    regV = /ipod|ipad|iphone/gi,
    result = ua.match(regV),
    userScale = "";
if (!result) {
    userScale = ",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0' + userScale + '">')

/* Custom script
 ========================================================*/
$(document).ready(function () {
    var camera = $('#camera');
    var owl = $('#owl');
    var owl2 = $('#owl_2');
    var isotope = $('.isotope');

    if(camera.length > 0){
        camera.camera(
            {
                autoAdvance: false,
                height: '31.25%',
                minHeight: '200px',
                pagination: false,
                thumbnails: false,
                playPause: false,
                hover: false,
                loader: 'none',
                navigation: true,
                navigationHover: false,
                mobileNavHover: false,
                fx: 'simpleFade'
            }
        );
    }

    if(owl.length > 0){
        owl.owlCarousel(
            {
                navigation: true,
                autoPlay: true,
                slideSpeed: 300,
                stopOnHover: true,
                pagination: false,
                paginationSpeed: 400,
                singleItem: true,
                mouseDrag: false,
                navigationText: ["", ""]
            }
        );
    }

    if(owl2.length > 0){
        owl2.owlCarousel(
            {
                navigation: true,
                autoPlay: true,
                slideSpeed: 300,
                stopOnHover: true,
                pagination: false,
                paginationSpeed: 400,
                singleItem: true,
                mouseDrag: false,
                navigationText: ["", ""]
            }
        );
    }
    var div=document.getElementsByClassName('activity');
    var input=document.getElementsByClassName('btn22');
    for(var i=0;i<input.length;i++){  //循环历遍onclick事件
        input[i].index=i; //input[0].index=0 index是自定义属性
        input[i].onclick=function(){
            for(var i=0;i<input.length;i++){  //循环历遍去掉button样式和把div隐藏
                div[i].style.display='none';
            }
            div[this.index].style.display='block';  //div显示 this.index是当前div 即div[0]之类的
        };
    }
    $('#btn1').click(function () {
        $('#sampledata1').bringins({
            "width":"60%",
            "position":"right",
            "closeButton":"rgb(255, 204, 0)"
        });
    });

    $('#btn2').click(function () {
        $('#sampledata2').bringins({
            "width":"60%",
            "position":"left",
            "closeButton":"rgb(255, 204, 0)"
        });
    });
    $('#btn3').click(function () {
        $('#sampledata3').bringins({
            "width":"40%",
            "position":"right",
            "closeButton":"rgb(255, 204, 0)"
        });
    });
    $('#btn4').click(function () {
        $('#sampledata4').bringins({
            "width":"60%",
            "position":"left",
            "closeButton":"rgb(255, 204, 0)"
        });
    });
});